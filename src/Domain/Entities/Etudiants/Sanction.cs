namespace Domain.Entities.Etudiants;

public enum Sanction
{
    Aucune,
    DecPreUniversitaire,
    DecTechnique
}