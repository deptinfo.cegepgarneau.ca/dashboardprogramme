namespace Domain.Entities.Etudiants;

public enum StatutImmigration
{
    ResidentPermanent,
    ResidentTemporaire,
    CitoyenCanadien
}