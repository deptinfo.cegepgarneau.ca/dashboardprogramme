﻿namespace Application.Services.Members.Exceptions;

public class GetAuthenticatedMemberOrganizationException(string message) : Exception(message);