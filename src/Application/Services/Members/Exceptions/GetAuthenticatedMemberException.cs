﻿namespace Application.Services.Members.Exceptions;

public class GetAuthenticatedMemberException(string message) : Exception(message);