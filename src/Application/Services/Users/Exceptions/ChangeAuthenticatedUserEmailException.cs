﻿namespace Application.Services.Users.Exceptions;

public class ChangeAuthenticatedUserEmailException(string message) : Exception(message);