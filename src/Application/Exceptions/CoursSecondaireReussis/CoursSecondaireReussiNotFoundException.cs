namespace Application.Exceptions.CoursSecondaireReussis;

public class CoursSecondaireReussiNotFoundException(string message) : Exception(message);