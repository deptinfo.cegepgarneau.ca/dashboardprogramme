namespace Application.Exceptions.IdentityError;

public class InvalidRoleNameException(string message) : Exception(message);