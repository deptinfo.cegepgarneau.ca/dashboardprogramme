namespace Application.Exceptions.Cours;

public class CoursNotFoundException(string message) : Exception(message);