﻿namespace Application.Exceptions.CoursAssistes;

public class CoursAssisteNotFoundException(string message) : Exception(message);