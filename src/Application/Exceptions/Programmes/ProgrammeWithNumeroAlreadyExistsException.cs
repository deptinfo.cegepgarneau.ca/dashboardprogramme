﻿namespace Application.Exceptions.Programmes;

public class ProgrammeWithNumeroAlreadyExists(string message) : Exception(message);