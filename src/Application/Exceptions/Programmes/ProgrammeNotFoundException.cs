﻿namespace Application.Exceptions.Programmes;

public class ProgrammeNotFoundException(string message) : Exception(message);