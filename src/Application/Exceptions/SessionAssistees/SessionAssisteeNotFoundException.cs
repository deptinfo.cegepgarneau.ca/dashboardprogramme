namespace Application.Exceptions.SessionAssistees;

public class SessionAssisteeNotFoundException(string message) : Exception(message);