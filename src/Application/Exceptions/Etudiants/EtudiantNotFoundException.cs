namespace Application.Exceptions.Etudiants;

public class EtudiantNotFoundException(string message) : Exception(message);