namespace Application.Exceptions.Etudiants;

public class EtudiantWithCodePermanentAlreadyExistsException(string message) : Exception(message);