namespace Application.Exceptions.SessionEtudes;

public class SessionEtudeNotFoundException(string message) : Exception(message);