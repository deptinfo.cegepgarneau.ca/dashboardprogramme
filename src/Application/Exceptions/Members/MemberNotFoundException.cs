﻿namespace Application.Exceptions.Members;

public class MemberNotFoundException(string message) : Exception(message);