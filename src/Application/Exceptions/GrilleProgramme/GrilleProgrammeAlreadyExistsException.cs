﻿namespace Application.Exceptions.GrilleProgramme;

public class GrilleProgrammeAlreadyExistsException(string message) : Exception(message);