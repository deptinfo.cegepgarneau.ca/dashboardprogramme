namespace Application.Interfaces.Services.SessionAssistees;

public record PortraitEtudiantAggregate(
    int[] SA,
    int[] RT,
    int[] R18
);