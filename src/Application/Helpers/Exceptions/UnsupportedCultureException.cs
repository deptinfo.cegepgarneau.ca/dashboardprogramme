﻿namespace Application.Helpers.Exceptions;

public class UnsupportedCultureException(string message) : Exception(message);