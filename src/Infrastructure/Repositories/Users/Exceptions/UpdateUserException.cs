﻿namespace Infrastructure.Repositories.Users.Exceptions;

public class UpdateUserException(string message) : Exception(message);