﻿namespace Infrastructure.Repositories.Users.Exceptions;

public class CreateUserException(string message) : Exception(message);