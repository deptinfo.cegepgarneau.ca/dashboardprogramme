﻿namespace Web.Features.Admins.Programmes.CreateProgramme;

public class CreateProgrammeRequest
{
    public string Nom { get; set; } = default!;
    public string Numero { get; set; } = default!;
}