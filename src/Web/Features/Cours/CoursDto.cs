﻿namespace Web.Features.Cours;

public class CoursDto
{
    public Guid Id { get; set; } = default!;
    public string Code { get; set; } = default!;
    public string Nom { get; set; } = default!;
}