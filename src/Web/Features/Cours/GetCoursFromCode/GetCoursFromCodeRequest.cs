namespace Web.Features.Cours.GetCoursFromCode;

public class GetCoursFromCodeRequest
{
    public string Code { get; set; } = default!;
}