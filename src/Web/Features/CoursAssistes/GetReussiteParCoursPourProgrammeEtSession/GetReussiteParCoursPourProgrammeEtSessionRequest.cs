﻿namespace Web.Features.CoursAssistes.GetReussiteParCoursPourProgrammeEtSession;

public class GetReussiteParCoursPourProgrammeEtSessionRequest
{
    public Guid IdProgramme { get; set; }
    public Guid IdSessionEtude { get; set; }
}