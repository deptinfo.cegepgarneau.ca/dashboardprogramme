﻿namespace Web.Features.CoursAssistes.GetReussiteParSessionPourProgramme;

public class GetReussiteParSessionPourProgrammeRequest
{
    public Guid Id { get; set; }
}