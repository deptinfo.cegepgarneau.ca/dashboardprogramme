﻿namespace Web.Features.Programmes.GetPortraitEtudiantAggregate;

public class GetPortraitEtudiantAggregateRequest
{
    public Guid IdProgramme { get; set; }
}