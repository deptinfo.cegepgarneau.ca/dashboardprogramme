﻿namespace Web.Features.Programmes.GetSpeAggregate;

public class GetSpeAggregateRequest
{
    public Guid IdProgramme { get; set; }
}