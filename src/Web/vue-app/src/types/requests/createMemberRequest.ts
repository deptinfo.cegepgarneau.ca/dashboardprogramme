﻿export interface ICreateMemberRequest {
    firstName?: string;
    lastName?: string;
    email?: string;
    password?: string;
    role?: string;
    programmes?: string[];
}