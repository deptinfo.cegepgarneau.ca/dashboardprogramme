﻿export interface IImportDataRequest {

    record?: string;
    sheetName?: string;
}