enum StatutImmigration {
    ResidentPermanent = "ResidentPermanent",
    ResidentTemporaire = "ResidentTemporaire",
    CitoyenCanadien = "CitoyenCanadien",
}