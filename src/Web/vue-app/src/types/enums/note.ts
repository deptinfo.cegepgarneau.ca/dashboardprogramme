export enum Note {
    Incomplet = "Incomplet",
    A = "A",
    B = "B",
    C = "C",
    D = "D",
    Echec = "Echec",
}