export enum Saison {
    Hiver = "Hiver",
    Ete = "Ete",
    Automne = "Automne",
}