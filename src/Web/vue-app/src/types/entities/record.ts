﻿export class RecordRequest {
    records?: Record<string, string>[];
    sheetName?: string;
}