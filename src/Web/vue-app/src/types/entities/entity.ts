import {Guid} from "@/types";

export interface IEntity {
    id?: Guid,
}