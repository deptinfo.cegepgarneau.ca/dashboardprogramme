import {IImportDataService} from "@/injection/interfaces";

export const TYPES = {
    IApiService: Symbol.for("IApiService"),
    IMemberService: Symbol.for("IMemberService"),
    IProgrammeService: Symbol.for("IProgrammeService"),
    IEtudiantService: Symbol.for("IEtudiantService"),
    ISessionEtudeService: Symbol.for("ISessionEtudeService"),
    ICoursAssisteService: Symbol.for("ICoursAssisteService"),
    ICoursService: Symbol.for("ICoursService"),
    ICorrelationsServices: Symbol.for("ICorrelationsServices"),
    ISPEQueryService: Symbol.for("ISPEQueryService"),
    IImportDataService: Symbol.for("IImportDataService"),
    IPortraitEtudiantService: Symbol.for("IPortraitEtudiantService"),
    AxiosInstance: Symbol.for("AxiosInstance")
};
